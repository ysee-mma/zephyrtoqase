var path = require('path');
var fs = require('fs'),
    xml2js = require('xml2js');

const testFolder = path.resolve(__dirname+'/zephyr/');
json = BuildQaseFilejson();

console.log('path '+testFolder);

var cases_pointer = 0;
fs.readdir(testFolder, (err, files) => {
  files.forEach(file => {
    if (file.endsWith(".xml")) {
        console.log("working with " + file);
        parsexml(testFolder +'/'+ file);
    }
  });
});
function parsexml (file){
    var parser = new xml2js.Parser();
    fs.readFile(file, function(err, data) {
    
        parser.parseString(data, function (err, result) {
            try{
            console.dir(result);
            console.log('parsing is Done for '+file);
            //json = BuildQaseBlankjson();
            //console.log('blank json '+JSON.stringify(json));
            //console.log(JSON.stringify(result));
            var xpath = require("xml2js-xpath");
            var steps = xpath.find(result, "//step");
            if (steps==null) return;
            console.log(steps.length +" steps found in " +file);
            var item = xpath.find(result, "//item");
            var zephyr_desc = item[0].description;
            //var steps = result.rss.channel[0].item[0].customfields[0].customfield[5].customfieldvalues[0].steps[0].step;
            var test_case_title = item[0].title + "";
            var test_case_desc = zephyr_desc + "";
            console.log("json test_case_desc "+JSON.stringify(test_case_desc));
            console.log('title for testcase : '+test_case_title);
            var case_obj = BuildQaseBlankCasejson();
            case_obj.title = test_case_title.replace(/\[Example-\d+\]/,"");
            case_obj.description = test_case_title +" — from old jira+zephyr \n"+test_case_desc.replace(/<\/?[^>]+>/gi, '');
            //json.suites[0].cases[0].title = test_case_title +"";
            //var step_nmbr = 0;
            for (var i = 0; i < steps.length; i++) {
                var step_def = steps[i].step + "";
                var res_def  = steps[i].result + "";
//                var parts = step_def.match(/\d\.\D+/g);
//                if (parts!=null) step_def = parts.join("\n");
//                res_def = res_def.replace(/\*/g,"\n\t*");
//                parts = res_def.match(/\d\.\D+/g);
//                if (parts!=null) res_def = parts.join("\n");
                //console.log('step '+(i+1)+': '+step_def);
                //console.log('step '+(i+1)+': result '+res_def);
                var step_obj = {
                    "position": 0,
                    "action": "",
                    "expected_result": "",
                    "data": ""
                };
                step_obj.position = (i+1);
                step_obj.action = step_def+"";
                step_obj.expected_result = res_def+"";
                step_obj.data = "";
                //json.suites[0].cases[0].steps.push(step_obj);
    //                json.cases[0].steps.push(step_obj);
                case_obj.steps.push(step_obj);
                //console.log("case "+JSON.stringify(case_obj));
             }
             json.cases.push(case_obj);
             //console.log("json iteration "+JSON.stringify(json));
             console.log('final json '+JSON.stringify(json));
             fs.writeFile (testFolder+"jira-zephyr-import.json", JSON.stringify(json), function(err) {
                if (err) throw err;
                console.log("rewrited file "+testFolder+" jira-zephyr-import.json");
            }
             );
            }catch(e){
                console.log('error file '+file.replace(".xml","")+".json" + e + "\n raw data: "+JSON.stringify(result));
            };
        });
    });
};
function BuildQaseFilejson(){
    var blank3 = {
        "cases":[]
    }
    return blank3;
}
function BuildQaseBlankCasejson(){
    var blank2 = {
            "title": "Last 10 minutes with realtime only gggggg",
            "description": "",
            "preconditions": null,
            "postconditions": null,
            "priority": "undefined",
            "severity": "undefined",
            "behavior": "undefined",
            "type": "other",
            "automation": "is-not-automated",
            "status": "actual",
            "milestone": null,
            "custom_fields": [],
            "steps": [
            ]
  }
      return blank2;
}